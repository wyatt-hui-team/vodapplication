'use strict';

const cluster = require('cluster'),
    stopSignals = [
        'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
        'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
    ],
    env = process.env,
    production = env.NODE_ENV == 'production',
    nconf = require('nconf');

let stopping = false;

cluster.on('disconnect', function(worker) {
  if (production) {
    if (!stopping) {
      cluster.fork();
    }
  } else {
    process.exit(1);
  }
});

if (cluster.isMaster) {
  const workerCount = env.NODE_CLUSTER_WORKERS || 4;
  console.log(`Starting ${workerCount} workers...`);
  for (let i = 0; i < workerCount; i++) {
    cluster.fork();
  }
  if (production) {
    stopSignals.forEach(function (signal) {
      process.on(signal, function () {
        console.log(`Got ${signal}, stopping workers...`);
        stopping = true;
        cluster.disconnect(function () {
          console.log('All workers stopped, exiting.');
          process.exit(0);
        });
      });
    });
  }
} else {
    var appPromise = require('./app');
    appPromise.spread(function (config, expressApp) {
        nconf.argv().env().file({file: 'config/' + env.NODE_ENV + '.json'});
        if (env.OPENSHIFT_MONGODB_DB_PASSWORD) {
            nconf.set('mongodb:connection_string', env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
                env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
                env.OPENSHIFT_MONGODB_DB_HOST + ':' +
                env.OPENSHIFT_MONGODB_DB_PORT + '/' +
                env.OPENSHIFT_APP_NAME);
        }
        var host = env.NODE_IP || nconf.get('host');
        var port = env.NODE_PORT || nconf.get('port');
        return expressApp.listenAsync(port, host).then(function (server) {
            console.log('[%s] Listening on http://%s:%d', expressApp.settings.env, host, port);
            return [config, expressApp, server];
        })

    });
}
