angular.module('webApp').controller('IndexCtrl', ["$scope", "$http", "$timeout",
    function ($scope, $http, $timeout) {
        (function getVideo() {
            $http({
                method: 'GET',
                url: 'https://demo2697834.mockable.io/movies'
            }).then(function setEntries(res) {
                return $scope.entries = _.reduce(res.data.entries, function (memo, entry) {
                        entry.images && entry.images[0] && entry.images[0].url && memo.push(entry);
                        return memo;
                    }, []) || undefined;
            }, function (res) {
                console.log(res);
            }).then(function setCarousel3d(entries) {
                if (!$scope.entries) {
                    return;
                }
                $scope.carousel3dOptions = {
                    visible: 10,
                    perspective: 35,
                    startSlide: 0,
                    border: 3,
                    dir: 'rtl',
                    height: _.first($scope.entries).images[0].height,
                    width: _.first($scope.entries).images[0].width,
                    controls: true
                };
                return entries;
            }).then(function setVideogular(entries) {
                if (!$scope.entries) {
                    return;
                }
                function selectVideo(index) {
                    var promise = (function updateVideogular() {
                        $('.videogular-container').hide().slideDown("slow");
                        $('html,body').animate({scrollTop: 0}, 'fast');
                        return $timeout(function () {
                            $scope.videogularConfig = {
                                sources: [{
                                    src: $scope.entries[index].contents[0].url,
                                    type: "video/mp4"
                                }],
                                theme: "components/videogular-themes-default/videogular.css",
                                plugins: {
                                    poster: $scope.entries[index].images[0].url
                                }
                            };
                        });
                    })();
                    (function updateHistory() {
                        $scope.history.unshift($scope.entries[index]);
                        $scope.historyContent = $scope.entries[index];
                        $http({
                            method: 'POST',
                            url: 'rest/history/update',
                            data: $scope.entries[index]
                        });
                    })();
                    return promise;
                }

                function playVideogular() {
                    $scope.videogularAPI && $timeout(function () {
                        $scope.videogularAPI.play();
                    });
                }

                $scope.slideChanged = function (index) {
                    $scope.selectedSlideIndex = index;
                };
                $scope.selectedClick = function (index) {
                    selectVideo(index).then(playVideogular);
                };
                $(document).keydown(function (event) {
                    if (event.which == 37) {
                        $('.carousel-3d-next').click();
                        event.preventDefault();
                    }
                    if (event.which == 39) {
                        $('.carousel-3d-prev').click();
                        event.preventDefault();
                    }
                    if (event.which == 13) {
                        selectVideo($scope.selectedSlideIndex).then(playVideogular);
                        event.preventDefault();
                    }
                });
                $scope.onPlayerReady = function (API) {
                    $scope.videogularAPI = API;
                    playVideogular();
                };
                return entries;
            });
        })();
        (function getHistory() {
            $http({
                method: 'GET',
                url: 'rest/history/get'
            }).then(function setHistory(res) {
                $scope.history = res.data && res.data.entries ? res.data.entries : [];
            });
        })();
    }]);
