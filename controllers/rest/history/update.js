'use strict';
var MongoClient = require('mongodb').MongoClient;
var nconf = require('nconf');
module.exports = function (router) {
    router.post('/', function (req, res) {
        MongoClient.connect('mongodb://' + nconf.get('mongodb').connection_string, function (err, db) {
            var history = db.collection("history");
            history.findOne({sessionID: req.sessionID}, function (err, result) {
                function resJson(err, result) {
                    db.close();
                    res.json(result);
                }

                if (result) {
                    result.entries.unshift(req.body);
                    history.update({sessionID: req.sessionID}, {$set: {entries: result.entries}}, resJson);
                } else {
                    history.insert({sessionID: req.sessionID, entries: [req.body]}, resJson);
                }
            });
        });
    });
};
