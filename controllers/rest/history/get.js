'use strict';
var nconf = require('nconf');

var MongoClient = require('mongodb').MongoClient;
module.exports = function (router) {
    router.get('/', function (req, res) {
        MongoClient.connect('mongodb://' + nconf.get('mongodb').connection_string, function (err, db) {
            db.collection("history").findOne({sessionID: req.sessionID}, function (err, result) {
                res.json(result);
            });
        });
    });
};
